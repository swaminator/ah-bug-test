import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>AppHub Bug Test 2</h1>
        <p>Test repo to demonstrate an issue with automatic deploys in private gitlab repositories.</p>
      </div>
    );
  }
}

export default App;
